# Codeberg page

https://epinez.codeberg.page/

## Subz

1. Built with the following flag: `ionic build --prod -- --base-href /projects/subz/`
2. Changed i18n path in `main-es2015-xxx.js` to `/projects/subz/assets/i18n/` manually

## Energize

1. Built with `flutter build web`
2. Change href in `index.html` like this: `<base href="/projects/energize/">`